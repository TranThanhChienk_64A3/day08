<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../day07/register.css">
    <style>
        .add-button {
            background: rgb(46, 117, 182);
            padding: 8px 10px;
            border: solid 2px rgb(100, 145, 184);
            width: 13%;
            display: block;
            color: white;
            border-radius: 6px;
            margin: 40px auto 0 auto;
        }

        .add {
            margin-left: 80%;
            margin-top: -30px;
        }

        .col1 {
            width: 8%;
            margin-left:-20px;
            padding: 5px;
        }

        .col2 {
            width: 35%;
            padding: 5px;

        }

        .col3 {
            width: 35%;
            padding: 5px;
            margin-left:-20px;
        }

        .col4 {
            padding: 5px;
        }

        button {
            margin-right: 2px;
            background-color: rgb(87, 137, 245);
            border: 1px solid rgb(87, 137, 245);
            color: #ffff;
        }
        .btn_ser{
            margin-left: 25%;
        }

    </style>
</head>

<body>
    <?php
    $khoa = array(
        'MAT' => 'Khoa học máy tính',
        'KDL' => 'Khoa học vật liệu'
    );
    
    session_start(); 
    $pkhoa = $keyword ="";
    if(!empty($_SESSION["pkhoa"])){
        $pkhoa = $_SESSION["pkhoa"];
    }
    if(!empty($_SESSION["keyword"])){
        $keyword= $_SESSION["keyword"];
    }
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $_SESSION = $_POST;
        $pkhoa = $_SESSION["pkhoa"];
        $keyword = $_SESSION["keyword"];
    }

    ?>
    <form method="POST" enctype="multipart/form-data" action="<?php
                                                                echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">

        <div class="elements">
            <label style="padding: 8px 10px; display: block;margin-right:18px">Khoa</label>
            <select name="pkhoa" id="pkhoa" class="input2 khoa">
                <option value=""></option>
                <?php
                foreach ($khoa as $key => $value) {
                    echo "\t<option value=\"{$value}\"";
                    if ($pkhoa == $value){
                        echo "selected";
                    }
                    echo ">{$value}</option>\n";
                };
                ?>
            </select>
        </div>
        <div class="elements">
            <label style="padding: 8px 10px; display: block;">Từ khoá</label>
            <input type="text" name="keyword" class="input2 khoa" id="keyword" value="<?php echo $keyword ?>"/>

        </div>
        <div class="btn_ser">
            <input type="button" id="remove" class="label-input" style="margin-top: 20px; border-radius: 5px"value="Xoá">
            <button style="margin-top:-40px" class="register">Tìm kiếm</button>
        </div>
        <p class="num"> Số sinh viên tìm thấy: XXX</p>
        <a href="../day07/register.php"><input class="add-button add" type="button" value="Thêm"></a>

        <table>
            <tr>
                <th class="col1">
                    <p>No</p>
                </th>
                <th class="col2">
                    <p>Tên sinh viên</p>
                </th>
                <th class="col3">
                    <p>Khoa</p>
                </th>
                <th class="col4">
                    <p>Action</p>
                </th>
            </tr>
            <tr>
                <td class="col1">
                    <p>1</p>
                </td>
                <td class="col2">
                    <p>Nguyễn Văn A</p>
                </td>
                <td class="col3">
                    <p>Khoa học máy tính</p>
                </td>
                <td class="col4"><button>Xoá</button><button>Sửa</button></td>
            </tr>
            <tr>
                <td class="col1">
                    <p>2</p>
                </td>
                <td class="col2">
                    <p>Trần Thị B</p>
                </td>
                <td class="col3">
                    <p>Khoa học máy tính</p>
                </td>
                <td class="col4"><button>Xoá</button><button>Sửa</button></td>
            </tr>
            <tr>
                <td class="col1">
                    <p>3</p>
                </td>
                <td class="col2">
                    <p>Nguyễn Hoàng C</p>
                </td>
                <td class="col2">
                    <p>Khoa học vật liệu</p>
                </td>
                <td class="col4"><button>Xoá</button><button>Sửa</button></td>
            </tr>
            <tr>
                <td class="col1">
                    <p>4</p>
                </td>
                <td class="col2">
                    <p>Đinh Quang D</p>
                </td>
                <td class="col3">
                    <p>Khoa học vật liệu</p>
                </td>
                <td class="col4"><button>Xoá</button><button>Sửa</button></td>
            </tr>

        </table>
    </form>
    <script>
        $(document).ready(function(){
            $("#remove").click(function(){
                localStorage.clear();
                $("#pkhoa").val('');
                $("#keyword").val('');
            });
        });
    </script>

</body>

</html>